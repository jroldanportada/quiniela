He planteado el trabajo de manera que se descargue el fichero de resultados de la quiniela únicamente en los formatos .json y .xml, de modo que si se introduce una ruta errónea con un fichero con otra extensión, esta será modificada por la extensión marcada en las opciones de arriba de la aplicación. Si hubiera algún fichero sin extensión, se le añadirá la extensión marcada en las opciones.

Seguidamente, la aplicación intentará descargar mediante RestClient el fichero de resultados y el de apuestas del servidor indicado en cada ruta. Se enviará al servidor la petición de crear un fichero en el directorio indicado en la ruta de AciertosYPremios en formato JSON o XML dependiendo de la opción marcada anteriormente.

En dicho fichero, aparecerán las apuestas escrutadas junto con las estadísticas de los premios obtenidos.

La aplicación identificará la última jornada si encuentra alguno de los partidos sin puntuar, es decir, con la cadena vacía "", siendo la última jornada la anterior a esta.

Entre algunas mejoras que he introducido, están:

1. La modificación de las extensiones de los ficheros en función de las opciones introducidas.
2. Si falla un fichero JSON debido a su formato, se tiene en cuenta que puedan existir cadenas "basura" que hayan sido incluidas en el fichero resultante de la conversión del fichero XML a JSON por parte del usuario o del administrador del servidor. Para ello, la aplicación consta de un array interno con posibles string que pudiera contener el fichero JSON que se vaya a descargar y se probará cada una de las posibilidades contempladas en este array de string hasta que pueda escrutarse el fichero o se lance una excepción indicando que el fichero JSON ha fallado.